import 'dart:io';

void main() {
  processDrink();
  processSweet();
  processPayment();
}

void chooseCoffee() {
  print("Menu:");
  print("Espresso 40 baht");
  print("Latte 45 baht");
  print("Mocha 45 baht");
  print('\n');
  print("Choose your coffee:");
  var coffee = stdin.readLineSync();
}

void chooseTea() {
  print("Menu:");
  print("Matcha Latte 45 baht");
  print("Thai Milk Tea 45 baht");
  print("Tea 40 baht");
  print('\n');
  print("Choose your tea:");
  var tea = stdin.readLineSync();
}

void chooseMilk() {
  print("Menu:");
  print("Caramel Milk 45 baht");
  print("Pink Milk 45 baht");
  print("Cocoa 45 baht");
  print('\n');
  print("Choose your milk:");
  var milk = stdin.readLineSync();
}

void chooseSoda() {
  print("Menu:");
  print("Plum Soda 40 baht");
  print("Strawberry Soda 45 baht");
  print("Pepsi 40 baht");
  print('\n');
  print("Choose your soda:");
  var soda = stdin.readLineSync();
}

void processDrink() {
  print("Choose a type of drink:");
  print("Coffee");
  print("Tea");
  print("Milk");
  print("Soda");
  print('\n');
  var type = stdin.readLineSync();
  switch (type) {
    case "Coffee":
      {
        chooseCoffee();
      }
      break;
    case "Tea":
      {
        chooseTea();
      }
      break;
    case "Milk":
      {
        chooseMilk();
      }
      break;
    case "Soda":
      {
        chooseSoda();
      }
      break;
  }
}
void processSweet() {
  print("Choose sweetness level of drink");
  print("No sugar");
  print("Less sweet");
  print("Sweet");
  print("Very sweet");
  print('\n');
  var level = stdin.readLineSync();
  switch (level) {
    case "No sugar":
      {
        print("No sugar");
      }
      break;
    case "Less sweet":
      {
        print("Less sweet");
      }
      break;
    case "Sweet":
      {
        print("Sweet");
      }
      break;
    case "Very sweet":
      {
        print("Very sweet");
      }
      break;
  }
}

void processPayment() {
  print("Choose your payment");
  print("Cash");
  print("Credit");
  print("Truemoney Wallet");
  print("Shopee Pay");
  print('\n');
  var pay = stdin.readLineSync();
  switch (pay) {
    case "Cash":
      {
        print("Pay by Cash");
      }
      break;
    case "Credit":
      {
        print("Pay by Credit");
      }
      break;
    case "Truemoney Wallet":
      {
        print("Pay by Truemoney Wallet");
      }
      break;
    case "Shopee Pay":
      {
        print("Pay by Shopee Pay");
      }
      break;
  }
}